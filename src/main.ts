import jQuery from 'jquery';
import BootstrapVue from 'bootstrap-vue';
import Vue from 'vue';
import LoadingIndicatorApp from './LoadingIndicatorApp.vue';
import VueI18n from 'vue-i18n';
import { LanguageUtil } from '@coscine/app-util';


Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(VueI18n);


jQuery(() => {
    const i18n = new VueI18n({
      locale: LanguageUtil.getLanguage(),
      messages: coscine.i18n.loadingindicator,
      silentFallbackWarn: true,
    });
    new Vue({
      i18n,
      render: (h) => h(LoadingIndicatorApp),
    }).$mount('loadingindicator');
});


